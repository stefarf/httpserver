package httpserver

import (
	"github.com/rs/cors"
	"net/http"
)

var CorsAllowAll Middleware = cors.New(cors.Options{
	AllowedOrigins: []string{"*"},
	AllowedHeaders: []string{"*"},
	AllowedMethods: []string{"GET", "POST", "PUT", "DELETE"},
}).Handler

func FileHandler(fileName string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, fileName)
	})
}

func DirHandler(fileDir string) http.Handler {
	return http.FileServer(http.Dir(fileDir))
}
