package main

import (
	"bitbucket.org/stefarf/httpserver"
	"bitbucket.org/stefarf/iferr"
	"fmt"
	"github.com/rs/cors"
	"net/http"
	"time"
)

func main() {
	// test1()
	test2()
}

func testClient(path string) {
	time.Sleep(time.Millisecond * 10)
	res, err := http.Get("http://localhost:8080" + path)
	iferr.Panic(err)
	res.Body.Close()
}

func mycors() httpserver.Middleware {
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedHeaders: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE"},
	})
	return c.Handler
}

func mdw1(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("mdw1")
		next.ServeHTTP(w, r)
	})
}

func mdw2(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("mdw2")
		next.ServeHTTP(w, r)
	})
}

func mdw3(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("mdw3")
		next.ServeHTTP(w, r)
	})
}

func h1(w http.ResponseWriter, r *http.Request) { fmt.Println("Handler 1") }
func h2(w http.ResponseWriter, r *http.Request) { fmt.Println("Handler 2") }
func h3(w http.ResponseWriter, r *http.Request) { fmt.Println("Handler 3") }
