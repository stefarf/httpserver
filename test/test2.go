package main

import (
	"bitbucket.org/stefarf/httpserver"
	"net/http"
)

func test2() {
	svr := httpserver.New()
	svr.Use(mdw1)
	mx1 := svr.NewMux("/mux1/")
	mx2 := svr.NewMux("/mux2/")

	mx1.Use(mdw2)
	mx1.Handle("/", http.HandlerFunc(h1))
	mx1.Handle("/satu", http.HandlerFunc(h2))

	mx2.Use(mdw3)
	mx2.Handle("/", http.HandlerFunc(h3))

	go svr.Run(":8080")
	testClient("/mux1/satu")
	testClient("/mux2/")
}
