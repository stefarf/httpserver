package main

import (
	"bitbucket.org/stefarf/httpserver"
	"net/http"
)

func test1() {
	svr := httpserver.New()
	svr.Use(mdw1)
	svr.Use(mdw2)
	svr.NewHandler("/", http.HandlerFunc(h1))
	go svr.Run(":8080")
	testClient("/mux1/satu")
}
