package main

import (
	"bitbucket.org/stefarf/httpserver"
	"bitbucket.org/stefarf/iferr"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

func main() {
	go runServer()
	res, err := http.Get("http://localhost:8080/mux1/satu")
	iferr.Panic(err)
	res.Body.Close()
}

func runServer() {
	main := http.NewServeMux()
	var then1, then2 http.Handler

	//
	// mux 1
	//

	mx1 := http.NewServeMux()
	mx1.Handle("/", http.HandlerFunc(h1))
	mx1.Handle("/satu", http.HandlerFunc(h2))
	mx1.Handle("/dua", http.HandlerFunc(h3))

	// use middlewares
	mdws := []httpserver.Middleware{mdw1, mdw2}
	var then http.Handler
	then = mx1
	for i := len(mdws) - 1; i >= 0; i-- {
		then = mdws[i](then)
	}

	then1 = then

	//
	// mux 2
	//

	api := httprouter.New()
	api.GET("/api/1/:id", api1)
	api.GET("/api/2/:id", api2)

	then2 = api

	//

	main.Handle("/mux1/", http.StripPrefix("/mux1", then1))
	main.Handle("/mux2/", http.StripPrefix("/mux2", then2))

	http.ListenAndServe(":8080", main)
}

func mdw1(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("mdw1")
		next.ServeHTTP(w, r)
	})
}

func mdw2(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("mdw2")
		next.ServeHTTP(w, r)
	})
}

func h1(w http.ResponseWriter, r *http.Request) { fmt.Println("Handler 1") }
func h2(w http.ResponseWriter, r *http.Request) { fmt.Println("Handler 2") }
func h3(w http.ResponseWriter, r *http.Request) { fmt.Println("Handler 3") }

func api1(w http.ResponseWriter, r *http.Request, p httprouter.Params) {}
func api2(w http.ResponseWriter, r *http.Request, p httprouter.Params) {}
