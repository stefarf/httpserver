package httpserver

type (
	Return interface {
		Type() ReturnType
	}

	ReturnType byte

	ReturnRedirect    struct{ URL string }
	ReturnServerError struct{ Error, Cause string }
	ReturnClientError struct{ Error, Cause string }
	ReturnUnauthorize struct{ Error, Cause string }
)

const (
	ReturnTypeRedirect ReturnType = iota
	ReturnTypeServerError
	ReturnTypeClientError
	ReturnTypeUnauthorize
)

func (r ReturnRedirect) Type() ReturnType    { return ReturnTypeRedirect }
func (r ReturnServerError) Type() ReturnType { return ReturnTypeServerError }
func (r ReturnClientError) Type() ReturnType { return ReturnTypeClientError }
func (r ReturnUnauthorize) Type() ReturnType { return ReturnTypeUnauthorize }
