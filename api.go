package httpserver

import (
	"bitbucket.org/stefarf/auth"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

type (
	ServerAPI struct {
		r    *httprouter.Router
		mdws []Middleware
		key  string
	}
)

func (sva *ServerAPI) Use(mdw Middleware)  { sva.mdws = append(sva.mdws, mdw) }
func (sva *ServerAPI) chain() http.Handler { return chain(sva.mdws, sva.r) }

func (sva *ServerAPI) GET(path string, handler func(ctx *Context)) {
	sva.register(sva.r.GET, path, handler)
}
func (sva *ServerAPI) POST(path string, handler func(ctx *Context)) {
	sva.register(sva.r.POST, path, handler)
}
func (sva *ServerAPI) PUT(path string, handler func(ctx *Context)) {
	sva.register(sva.r.PUT, path, handler)
}
func (sva *ServerAPI) DELETE(path string, handler func(ctx *Context)) {
	sva.register(sva.r.DELETE, path, handler)
}

func (sva *ServerAPI) register(method func(path string, handle httprouter.Handle), path string, handler func(ctx *Context)) {
	method(path, func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		ctx := &Context{w: w, r: r, p: p, a: auth.New(sva.key)}
		handler(ctx)
		switch ctx.ret.(type) {
		case nil:
		case ReturnRedirect:
			u := ctx.ret.(ReturnRedirect).URL
			fmt.Println("Redirect to:", u)
			http.Redirect(w, r, u, http.StatusSeeOther)
		case ReturnServerError:
			r := ctx.ret.(ReturnServerError)
			msg := r.Error + "; " + r.Cause
			fmt.Println("Internal server error:", msg)
			http.Error(w, msg, http.StatusInternalServerError)
		case ReturnClientError:
			r := ctx.ret.(ReturnClientError)
			msg := r.Error + "; " + r.Cause
			fmt.Println("Bad request error:", msg)
			http.Error(w, msg, http.StatusBadRequest)
		case ReturnUnauthorize:
			r := ctx.ret.(ReturnUnauthorize)
			msg := r.Error + "; " + r.Cause
			fmt.Println("Unauthorized error:", msg)
			http.Error(w, msg, http.StatusUnauthorized)
		}
	})
}
