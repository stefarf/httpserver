package httpserver

import "net/http"

type (
	ServerHandler struct {
		h    http.Handler
		mdws []Middleware
	}
)

func (svh *ServerHandler) Use(mdw Middleware)  { svh.mdws = append(svh.mdws, mdw) }
func (svh *ServerHandler) chain() http.Handler { return chain(svh.mdws, svh.h) }
