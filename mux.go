package httpserver

import "net/http"

type (
	ServerMux struct {
		m    *http.ServeMux
		mdws []Middleware
	}
)

func (svm *ServerMux) Use(mdw Middleware)                    { svm.mdws = append(svm.mdws, mdw) }
func (svm *ServerMux) Handle(pattern string, h http.Handler) { svm.m.Handle(pattern, h) }
func (svm *ServerMux) chain() http.Handler                   { return chain(svm.mdws, svm.m) }
