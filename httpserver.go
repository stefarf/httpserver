package httpserver

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

type (
	Server struct {
		mdws []Middleware
		bh   []baseHandler
	}

	Middleware func(next http.Handler) http.Handler

	baseHandler struct {
		b string
		f func() http.Handler
	}
)

func New() *Server { return &Server{} }

func (svr *Server) Use(mdw Middleware) { svr.mdws = append(svr.mdws, mdw) }

func (svr *Server) NewHandler(basePath string, h http.Handler) *ServerHandler {
	svh := &ServerHandler{h: h}
	svr.bh = append(svr.bh, baseHandler{b: basePath, f: svh.chain})
	return svh
}

func (svr *Server) NewMux(basePath string) *ServerMux {
	if basePath[len(basePath)-1] != '/' {
		basePath += "/"
	}
	svm := &ServerMux{m: http.NewServeMux()}
	svr.bh = append(svr.bh, baseHandler{b: basePath, f: svm.chain})
	return svm
}

func (svr *Server) NewAPIMux(basePath string) *ServerAPI {
	if basePath[len(basePath)-1] != '/' {
		basePath += "/"
	}
	sva := &ServerAPI{r: httprouter.New()}
	svr.bh = append(svr.bh, baseHandler{b: basePath, f: sva.chain})
	return sva
}

func (svr *Server) Run(addr string) {
	mux := http.NewServeMux()
	for _, bh := range svr.bh {
		mux.Handle(bh.b, http.StripPrefix(bh.b[:len(bh.b)-1], bh.f()))
	}
	http.ListenAndServe(addr, chain(svr.mdws, mux))
}

func chain(mdw []Middleware, then http.Handler) http.Handler {
	for i := len(mdw) - 1; i >= 0; i-- {
		then = mdw[i](then)
	}
	return then
}
