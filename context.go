package httpserver

import (
	"bitbucket.org/stefarf/auth"
	"bitbucket.org/stefarf/iferr"
	"encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	"net/http"
	"strconv"
)

type (
	Context struct {
		ret Return

		w http.ResponseWriter
		r *http.Request
		p httprouter.Params
		a *auth.Auth

		Email string
	}
)

const (
	jwtHeader       = "X-Token"
	jsonContentType = "application/json"
)

func (ctx *Context) ParamUint64(name string, i *uint64) {
	if ctx.ret != nil {
		return
	}
	id, err := strconv.ParseInt(ctx.p.ByName(name), 10, 64)
	if err != nil {
		ctx.ret = ReturnClientError{
			Error: fmt.Sprintf("Error parse url parameter %s to int", name),
			Cause: err.Error()}
		return
	}
	*i = uint64(id)
}

func (ctx *Context) AuthJWT() {
	if ctx.ret != nil {
		return
	}
	token := ctx.r.Header.Get(jwtHeader)
	email, err := ctx.a.VerifyAndReadToken(token)
	if err != nil {
		ctx.ret = ReturnUnauthorize{
			Error: fmt.Sprintf("Invalid token: '%s'", token),
			Cause: err.Error()}
		return
	}
	ctx.Email = email
}

func (ctx *Context) ReadJSON(v interface{}) {
	if ctx.ret != nil {
		return
	}

	// Validate Content-Type
	ct := ctx.r.Header.Get("Content-Type")
	if ct[:len(jsonContentType)] != jsonContentType {
		msg := "Expect Content-Type: application/json"
		fmt.Println("Bad request:", ct, msg)
		ctx.ret = ReturnClientError{Error: msg}
		return
	}

	// Read json data
	b, err := ioutil.ReadAll(ctx.r.Body)
	if err != nil {
		fmt.Println("Bad request:", err)
		ctx.ret = ReturnClientError{Error: "Error reading http request", Cause: err.Error()}
		return
	}
	fmt.Printf("Read: [%s]\n%s\n", ct, b)

	err = json.Unmarshal(b, v)
	if err != nil {
		fmt.Println("Error json format:", err.Error())
		ctx.ret = ReturnClientError{Error: "Error json format", Cause: err.Error()}
		return
	}
}

func (ctx *Context) WriteJSON(v interface{}) {
	if ctx.ret != nil {
		return
	}
	b, err := json.Marshal(v)
	iferr.Panic(err)
	ctx.w.Header().Set("Content-Type", jsonContentType)
	ctx.w.Write(b)
	fmt.Printf("Write: [%s]\n%s\n", jsonContentType, b)
}

func (ctx *Context) Do(f func() Return) {
	if ctx.ret != nil {
		return
	}
	ctx.ret = f()
}
